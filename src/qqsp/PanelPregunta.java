/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package qqsp;

import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import static qqsp.DfButton.COLOR_DF_BG;
import static qqsp.DfButton.COLOR_DF_BGS;

/**
 *
 * @author ubuntu
 */
public class PanelPregunta extends JPanel {

    public static final int AYUDA_50_50 = 1;
    public static final int AYUDA_CAMBIO_PREGUNTA = 2;
    public static final int AYUDA_PUBLICO = 3;
    public static final int AYUDA_LLAMADA = 4;
    private JLabel lbPregunta;
    private DfButton[] btOpciones;
    private BufferedImage image;
    private Graphics2D g;

    public PanelPregunta() {
        initComponents();
    }

    private void initComponents() {
        lbPregunta = new JLabel();
        btOpciones = new DfButton[4];
        image = new BufferedImage(200, 100, BufferedImage.TYPE_INT_ARGB);
//        g = image.createGraphics();
//        g.setBackground(Color.blue);
//        g.fillRect(0, 0, 200, 100);

        for (int i = 0; i < btOpciones.length; i++) {
            btOpciones[i] = new DfButton();
//            btOpciones[i].setHorizontalAlignment(SwingConstants.LEFT);
//            btOpciones[i].setContentAreaFilled(false);
            btOpciones[i].setFocusPainted(false);

//                    btOpciones[i].
//                    btOpciones[i].
        }

        setLayout(new GridBagLayout());
        add(lbPregunta, new GridBagConstraints(1, 1, 10, 2, 1.0, 0.1, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(20, 15, 4, 15), 100, 100));
        add(btOpciones[0], new GridBagConstraints(0, 3, 2, 1, 1.0, 0.1, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(20, 30, 20, 30), 100, 100));
        add(btOpciones[1], new GridBagConstraints(6, 3, 2, 2, 1.0, 0.1, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(20, 30, 20, 30), 100, 100));
        add(btOpciones[2], new GridBagConstraints(0, 5, 2, 2, 1.0, 0.1, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(20, 30, 20, 30), 100, 100));
        add(btOpciones[3], new GridBagConstraints(6, 5, 2, 2, 1.0, 0.1, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(20, 30, 20, 30), 100, 100));
    }

    public void hacerPregunta(Pregunta pregunta) {
        lbPregunta.setText("<html><font color=blue, size=+5>" + pregunta.getPregunta() + "</font></html>");
        char ctLetra = 'A';
        for (int i = 0; i < pregunta.getOpciones().length; i++) {
            btOpciones[i].setText("<html><font color=orange size=+4>" + (ctLetra++) + ".   </font><font color=blue, size=+3>" + pregunta.getOpciones()[i] + "</font></html>");
        }
        System.err.println(btOpciones[0].getText());

    }

    private void usarAyuda(int ayuda) {
        switch (ayuda) {
            case AYUDA_50_50:

                break;
            case AYUDA_CAMBIO_PREGUNTA:

                break;
            case AYUDA_LLAMADA:

                break;
            case AYUDA_PUBLICO:

                break;
            default:
                throw new AssertionError();
        }

    }

    private boolean isCorrecto(Pregunta p, int respuesta) {
        return true;
    }

    

}
