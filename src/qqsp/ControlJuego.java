/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package qqsp;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashMap;
import org.w3c.dom.Document;

/**
 *
 * @author ubuntu
 */
public class ControlJuego {

    private ArrayList<Pregunta> preguntas;
    private HashMap<Integer, Integer> preguntasHechas;
    private Aplication app;
    private int numPreguntas;
    private Pregunta preguntaActual;
    private int nivel;
    private PropertyChangeSupport pcs;
    private ArrayList premios;

    public ControlJuego(Aplication app) {
        this.app = app;
        pcs = new PropertyChangeSupport(this);

        preguntasHechas = new HashMap<>();
        preguntas = new ArrayList<>();
        premios = new ArrayList();

        numPreguntas = 10;
        for (int i = 0; i < numPreguntas; i++) {
            premios.add((i + 1) * 1000);
        }
    }

    public final void cargarPreguntas() {
        try {
            Document doc = app.getXMLManager().getDocument("preguntas.xml");
            preguntas = app.getXMLManager().cargarPreguntasXML(doc);
            numPreguntas = preguntas.size();
        } catch (MalformedURLException ex) {
            GuiManager.showErrorMessage(null, "Error cargando las preguntas", "Error");
        }
    }

    public final Pregunta getPregunta() {
        int aleatorio = 0;
        int inf = numPreguntas;
        int cont = 0;
        boolean band = true;
        do {
            aleatorio = org.dzur.Mat.aleatorio(0, numPreguntas - 1);
            if (!preguntasHechas.isEmpty() || !preguntasHechas.containsKey(aleatorio)) {
                int num = preguntasHechas.get(aleatorio) == null ? 0 : preguntasHechas.get(aleatorio);
                preguntasHechas.put(aleatorio, num + 1);
                band = false;
            }
            System.err.println("ciclo:" + cont++);
        } while (band);
        preguntaActual = preguntas.get(aleatorio);
        return preguntaActual;
    }

    public void empezarJuego() {
        cargarPreguntas();

    }

    public boolean respuestaCorrecta(int respuesta) {
        return preguntaActual.getRespuesta() == respuesta;
    }

    public void avanzar() {
        nivel++;
        pcs.firePropertyChange("AVANZAR", 0, nivel);

    }

    public void addPropertyChangeListener(PropertyChangeListener pcl) {
        pcs.addPropertyChangeListener(pcl);
    }

    public void removePropertyChangeListener(PropertyChangeListener pcl) {
        pcs.removePropertyChangeListener(pcl);
    }

    public ArrayList getPremios() {
        return premios;
    }

    public Pregunta getPreguntaActual() {
        return preguntaActual;
    }

}
