/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package qqsp;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JLabel;

/**
 *
 * @author ubuntu
 */
public class Reloj extends JLabel implements Runnable {

    private double angulo = 270;
    private double incremento;
    private Thread hilo;
    private int contador;

    public Reloj() {
        incremento = 360 / 30;
        hilo = new Thread(this);
        setOpaque(true);
        setBackground(Color.cyan);
        System.err.println("creando reloj");
        repaint();
    }

    @Override
    public void paint(Graphics g) {
        System.err.println("pintando reloj");
        super.paint(g);
        Graphics2D g2 = (Graphics2D) g;
        g2.setColor(Color.orange);
        g2.fillOval(0, 0, 200, 200);
        g2.setColor(Color.red);
        g2.setStroke(new BasicStroke(2.0f));
        g2.drawOval(0, 0, 200, 200);
        g2.translate(100, 100);
        g2.setColor(Color.white);
        Point pnt = getCoordenadas();
        g2.drawLine(0, 0, pnt.x, pnt.y);
    }

    public Point getCoordenadas() {
        System.err.println(angulo);
        double angRad = Math.toRadians(angulo);
        int x = ((int) (100 * Math.cos(angRad)));
        int y = ((int) (100 * Math.sin(angRad)));
        System.err.println(x + "," + y);
        return new Point(x, y);

    }

    @Override
    public void run() {
        while (contador > 0) {
            angulo += incremento;

            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                Logger.getLogger(Reloj.class.getName()).log(Level.SEVERE, null, ex);
            }
            contador--;
            repaint();

        }

    }

    public void start(int contador) {
        this.contador = contador;
        hilo.start();
    }

}
