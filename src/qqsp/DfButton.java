/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package qqsp;

import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JButton;

/**
 *
 * @author ubuntu
 */
public class DfButton extends JButton implements MouseListener {

    public static final Color COLOR_DF_BG = new Color(255, 156, 22);
    public static final Color COLOR_DF_BGS = new Color(255, 206, 22);
    private boolean animando;

    public DfButton() {
        this(null, null);
    }

    public DfButton(String text, Icon icon) {
        super(text, icon);
        init();
    }

    private void init() {
        animando = false;
//        setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1));
        setBackground(COLOR_DF_BG);

        addMouseListener(this);

    }

    @Override
    public void mouseClicked(MouseEvent e) {
        Thread hilo = new Thread(new Runnable() {

            @Override
            public void run() {
                animando = true;
                setBackground(Color.BLUE);
                try {
                    Thread.sleep(1500);

                    boolean correcto = org.dzur.Mat.probabilidad(.5);
                    if (correcto) {

//                        for (int i = 0; i < 20; i++) {
//                            Thread.sleep(200);
//                            setBackground(i % 2 == 0 ? Color.green : Color.blue);
//                        }
                        setBackground(Color.green);
                    } else {

//                        for (int i = 0; i < 20; i++) {
//                            Thread.sleep(200);
//                            setBackground(i % 2 == 0 ? Color.red : Color.blue);
//                        }
                        setBackground(Color.red);
                    }
                } catch (InterruptedException ex) {
                    Logger.getLogger(DfButton.class.getName()).log(Level.SEVERE, null, ex);
                }
                animando = false;
            }
        });
        hilo.start();
    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {
        System.err.println("entered");
        setBackground(COLOR_DF_BGS);
        repaint();
    }

    @Override
    public void mouseExited(MouseEvent e) {
        if (!animando) {
            System.err.println("exited");
            setBackground(COLOR_DF_BG);
        }
    }

}
