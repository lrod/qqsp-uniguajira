/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package qqsp;

import java.awt.Image;
import java.io.*;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashMap;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

/**
 *
 * @author hp
 */
public class XMLManager {

    public static final String TAG_ROOT = "qqsp";
    public static final String TAG_ITEM = "item";
    public static final String TAG_PREGUNTA = "pregunta";
    public static final String TAG_OPCION1 = "opcion1";
    public static final String TAG_OPCION2 = "opcion2";
    public static final String TAG_OPCION3 = "opcion3";
    public static final String TAG_OPCION4 = "opcion4";
    public static final String TAG_RESPUESTA = "respuesta";
    public static final String TAG_NIVEL = "nivel";
    private static final String XML_VERSION = "1.0";
    private static final String XML_ENCODING = "ISO-8859-1";
    private static final String JAVA_ENCODING = "8859_1";

    private XMLManager() {

    }

    public static XMLManager getInstance() {
        return XMLManagerHolder.INSTANCE;
    }

    private static class XMLManagerHolder {

        private static final XMLManager INSTANCE = new XMLManager();
    }

    public ArrayList cargarPreguntasXML(Document xmlDoc) throws MalformedURLException {

        ArrayList<Pregunta> list = new ArrayList<>();
        NodeList nodes = xmlDoc.getDocumentElement().getChildNodes();
        if (nodes != null && nodes.getLength() > 0) { // If there are some...
            Node elementNode = null;
            for (int i = 0; i < nodes.getLength(); ++i) {
                elementNode = nodes.item(i);
                switch (elementNode.getNodeName()) {
                    case TAG_ITEM:
                        Pregunta preg = getPregunta(elementNode);
                        if (preg != null) {
                            list.add(preg);
                        }
                        break;
                }
            }
        }
        return list;
    }

    public Document getDocument(String archivo) {
        Document doc = null;
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dbBuilder = dbFactory.newDocumentBuilder();
            doc = dbBuilder.parse(new File(archivo));
            doc.getDocumentElement().normalize();
        } catch (ParserConfigurationException | SAXException | IOException e) {
            System.out.println("exc: " + e.getMessage());
        }
        return doc;
    }

    private Pregunta getPregunta(Node elemento) {
        Pregunta preg = null;
        String[] opciones = new String[4];
        if (elemento != null) {
            preg = new Pregunta();
            NodeList nodes = elemento.getChildNodes();
            if (nodes.getLength() > 0) { // If there are some...
                for (int i = 0; i < nodes.getLength(); ++i) {
                    switch (nodes.item(i).getNodeName()) {
                        case TAG_PREGUNTA:
                            preg.setPregunta(getTagValue(TAG_PREGUNTA, (Element) elemento));
                            break;
                        case TAG_OPCION1:
                            opciones[0] = (getTagValue(TAG_OPCION1, (Element) elemento));
                            break;
                        case TAG_OPCION2:
                            opciones[1] = (getTagValue(TAG_OPCION2, (Element) elemento));
                            break;
                        case TAG_OPCION3:
                            opciones[2] = (getTagValue(TAG_OPCION3, (Element) elemento));
                            break;
                        case TAG_OPCION4:
                            opciones[3] = (getTagValue(TAG_OPCION4, (Element) elemento));
                            break;
                        case TAG_RESPUESTA:
                            preg.setRespuesta(Integer.valueOf(getTagValue(TAG_RESPUESTA, (Element) elemento)));
                            break;
                        default:
                        // System.err.println("Nodo invalido en <Pregunta>: " + aNodo);
                    }
                }
            }
            preg.setOpciones(opciones);
            if (elemento.hasAttributes()) {
                NamedNodeMap atr = elemento.getAttributes();
                preg.setNivel(Integer.parseInt(((Attr) (atr.getNamedItem(TAG_NIVEL))).getValue()));
            }
            System.err.println(preg.getPregunta());
            System.err.println(preg.getRespuesta());
        }
        return preg;
    }

    private String getTagValue(String string, Element elemento) {
        NodeList nList = elemento.getElementsByTagName(string).item(0).getChildNodes();
        Node nValue = (Node) nList.item(0);
        try {
            return nValue.getNodeValue();
        } catch (Exception e) {
            return "";
        }
    }
}
