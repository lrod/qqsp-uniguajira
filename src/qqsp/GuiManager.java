/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package qqsp;

import com.proyecto.gui.JStatusbar;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.Calendar;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.JToolBar;
import org.bx.gui.MyDialog;
import static org.dzur.gui.GuiUtil.centrarFrame;

/**
 *
 * @author ubuntu
 */
public class GuiManager {
    
    private static Aplication app;
    private PnPresentacion panelPresentacion;
    private MyDialog myDialog;
    private PanelPrincipal panelJuego;
    private PanelInstruccion panelInstruccion;
    
    private GuiManager() {
    }
    
    public static GuiManager getInstance(Aplication app) {
        GuiManager.app = app;
        return GUIManagerHolder.INSTANCE;
    }
    
    private static class GUIManagerHolder {
        
        private static final GuiManager INSTANCE = new GuiManager();
    }
    private WindowAdapter wHandler;
    private JFrame frame;
    private JSplitPane splitpane;
    private JToolBar toolbar;
    private JMenuBar menubar;
    private JStatusbar statusbar;
    private JPanel container;
    
    public void configurar() {
        setWaitCursor();
        centrarFrame(getFrame());
        
        getFrame().setTitle(Aplication.TITLE + " " + Aplication.VERSION);
//        getContenedor().add(getToolbar(), BorderLayout.NORTH);
        getContenedor().add(getSplitpane(), BorderLayout.CENTER);
        
        wHandler = new WindowHandler();

        //getSplitpane().setBottomComponent(getgEditor().getScroll());
        getSplitpane().setTopComponent(getPanelPresentacion());
//        getSplitpane().setTopComponent(getPanelVerifyUser());
//        getSplitpane().setTopComponent(getPanelTarjeton());
        getSplitpane().setResizeWeight(1.0);
//        getFrame().add(getMenu(), BorderLayout.NORTH);
        getFrame().add(getContenedor(), BorderLayout.CENTER);
        getFrame().addWindowListener(getwHandler());
        getFrame().setVisible(true);
        setDefaultCursor();
    }
    
    public void setWaitCursor() {
        Cursor waitCursor = Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR);
        getFrame().setCursor(waitCursor);
    }
    
    public void setDefaultCursor() {
        getFrame().setCursor(Cursor.getDefaultCursor());
    }
    
    public WindowAdapter getwHandler() {
        return wHandler;
    }
    
    public void agregarSplitPaneAbajo(JComponent componente) {
        getSplitpane().setBottomComponent(componente);
        getSplitpane().setDividerLocation(0.77);
    }
    
    public JFrame getFrame() {
        if (frame == null) {
            frame = new JFrame();
            frame.setLayout(new BorderLayout());
            frame.setIconImages(getListIconos());
//            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        }
        return frame;
    }
    
    public ArrayList<Image> getListIconos() {
        ArrayList list = new ArrayList<>();
//        list.add(app.imgMan.getBufImagen("img/Horario_128.png"));
//        list.add(app.imgMan.getBufImagen("img/Horario_256.png"));
        return list;
    }
    
    public JPanel getContenedor() {
        if (container == null) {
            container = new JPanel(new BorderLayout());
        }
        return container;
    }
    
    public JSplitPane getSplitpane() {
        if (splitpane == null) {
            splitpane = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
        }
        return splitpane;
    }
    
    private Component getPanelPresentacion() {
        if (panelPresentacion == null) {
            panelPresentacion = new PnPresentacion(app, app.getImgManager().getImagen("presentacion.jpg"));
        }
        return panelPresentacion;
    }
    
    private Component getPanelJuego() {
        if (panelJuego == null) {
            panelJuego = new PanelPrincipal(app, app.getControl());
        }
        return panelJuego;
    }
    
    public PanelJuego getPanelJuegos() {
        return (PanelJuego) getPanelJuego();
    }
    
    private Component getPanelInstruccion() {
        if (panelInstruccion == null) {
            panelInstruccion = new PanelInstruccion(app, app.getImgManager().getImagen("presentacion.jpg"));
        }
        return panelInstruccion;
    }
    
    private void mostrarAcercaDe() {
        String msg = Aplication.TITLE + " " + Aplication.VERSION;
        JOptionPane.showMessageDialog(null, getCopyright(), "Acerca de", JOptionPane.INFORMATION_MESSAGE);
    }
    
    public String getCopyright() {
        StringBuilder html = new StringBuilder();
        int year = Calendar.getInstance().get(Calendar.YEAR);
        
        html.append("<html>");
        html.append("<p><font color=blue size=+2>").append(Aplication.TITLE);
        html.append(" ").append(Aplication.VERSION).append("</font></p>");
//        html.append("<p align=center  size=+1>Luis J. Rodriguez</p><p></p>");

        html.append("<p> <font color = blue size = -1>Derechos reservados ").append(year).append("</font></p>");
        html.append("<html>");
        return html.toString();
    }
    
    public void showPreferences() {
        setWaitCursor();
        JDialog dialog = getDialog(true);
        int w = 550;
        int h = 440;
        dialog.setPreferredSize(new Dimension(w, h));
        dialog.setTitle("Cambiar preferencias.");
        dialog.pack();
        dialog.setLocationRelativeTo(getFrame());
        setDefaultCursor();
        dialog.setVisible(true);
    }
    
    void iniciarJuego() {
        showPanelJuego();
        
    }
    
    public void showPanelInstrucciones() {
        setWaitCursor();
        getSplitpane().setTopComponent(getPanelInstruccion());
        setDefaultCursor();
    }
    
    public void showPanelJuego() {
        setWaitCursor();
        getSplitpane().setTopComponent(getPanelJuego());
        setDefaultCursor();
    }
    
    public void showPanelPresentacion() {
        setWaitCursor();
        getSplitpane().setTopComponent(getPanelPresentacion());
        setDefaultCursor();
    }
    
    public MyDialog getDialog(boolean limpiar) {
        if (myDialog == null) {
            myDialog = new MyDialog(frame);
        }
        if (limpiar) {
            myDialog.getContentPane().removeAll();
        }
        return myDialog;
    }
    
    public MyDialog getDialog(JDialog parent) {
        MyDialog dialog = new MyDialog(parent);
        return dialog;
    }
    
    public void showError(String message) {
        JOptionPane.showMessageDialog(getFrame(), message, "Error", JOptionPane.ERROR_MESSAGE);
    }
    
    public static void showErrorMessage(Component parent, Object mensaje, String titulo) {
        JOptionPane.showMessageDialog(
                parent,
                mensaje,
                titulo,
                JOptionPane.ERROR_MESSAGE);
    }
    
    class WindowHandler extends WindowAdapter {
        
        @Override
        public void windowClosing(WindowEvent e) {
            app.salir(0);
        }
    }
    
}
