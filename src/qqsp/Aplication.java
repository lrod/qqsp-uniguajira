/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package qqsp;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import javax.swing.AbstractAction;
import javax.swing.ImageIcon;
import javax.swing.JButton;

/**
 *
 * @author ubuntu
 */
public class Aplication implements ActionListener, PropertyChangeListener {

    public static final String TITLE = "QQSP Uniguajira";
    public static final String VERSION = "";
    public static final String PREFERENCES = "";
    public static final String ACCION_CANCELAR_PANEL = "ac_cancelar_panel";
    public static final String ACCION_SALIR_APP = "ac_salir_app";
    public static final String AC_NUEVO_JUEGO = "ac_nuevo_juego";
    public static final String AC_INSTRUCCION_JUEGO = "ac_instruccion_juego";
    public static final String AC_MOSTRAR_PRESENTACION = "ac_mostrar_presentacion";

    private ArrayList<Pregunta> preguntas;
    private Ayuda[] ayudas;
    private ImageManager imgManager;
    private GuiManager guiManager;
    private Configuration configuration;
    private ProgAction acSalirApp;
    private String WORK_FOLDER;
    private ControlJuego control;
    private final XMLManager xmlManager;
    private Reproductor rep;

    public Aplication() {
        configuration = Configuration.getInstancia();
        imgManager = ImageManager.getInstance();
        guiManager = GuiManager.getInstance(this);
        xmlManager = XMLManager.getInstance();

        configWorkFolder();
        configuration.setPath(getDirTrabajo());
        configuration.load();

        rep = new Reproductor();
        control = new ControlJuego(this);

        initActions();

    }

    public void init() {
        loadImages();
        getGuiManager().configurar();
        rep.playSound("intro.mp3", true);
    }

    private void loadImages() {
        imgManager.getImagen("presentacion.jpg");
    }

    public ControlJuego getControl() {
        return control;
    }

    @Override

    public void actionPerformed(ActionEvent evt) {
        switch (evt.getActionCommand()) {
            case Aplication.ACCION_CANCELAR_PANEL:
                ((JButton) evt.getSource()).getRootPane().getParent().setVisible(false);
                break;
            case Aplication.AC_NUEVO_JUEGO:

                control.empezarJuego();
                getGuiManager().iniciarJuego();
                break;
            case Aplication.AC_INSTRUCCION_JUEGO:
                getGuiManager().showPanelInstrucciones();
                break;
            case Aplication.AC_MOSTRAR_PRESENTACION:
                getGuiManager().showPanelPresentacion();
                rep.playSound("intro.mp3", true);
                break;
        }
    }

    private void initActions() {

        acSalirApp = new ProgAction("Salir",
                new ImageIcon(imgManager.getImagen("img/exit.png", 25, 25)), "Salir de la aplicacion", 'x') {

                    public void actionPerformed(ActionEvent e) {
                        salir(0);
                    }
                };
        acSalirApp.setSmallIcon(new ImageIcon(imgManager.getImagen("gui/img/exit.png", 18, 18)));
        acSalirApp.setLargeIcon(new ImageIcon(imgManager.getImagen("gui/img/exit.png", 25, 25)));

    }

    public AbstractAction getAction(String action) {
        switch (action) {
            case ACCION_SALIR_APP:
                return acSalirApp;
            default:
                return null;
        }
    }

    public Reproductor getReproductor() {
        return rep;
    }

    public Configuration getConfiguration() {
        return configuration;
    }

    public ImageManager getImgManager() {
        return imgManager;
    }

    public GuiManager getGuiManager() {
        return guiManager;
    }

    public XMLManager getXMLManager() {
        return xmlManager;
    }

    public String getUserHome() {
        return System.getProperty("user.home");
    }

    public String getRunDir() {
        return System.getProperty("user.dir");
    }

    public String getDirDocuments() {
        Path dir = Paths.get(System.getProperty("user.home"), "");
        return dir.toString() + File.separator + "Documents";
    }

    public String getDirTrabajo() {
        return getUserHome() + File.separator + WORK_FOLDER;
    }

    public void salir(int i) {
        guiManager.getFrame().setVisible(false);
        guiManager.getFrame().dispose();
        configuration.savePreferences();
        configuration.save();
        System.exit(i);
    }

    private boolean configWorkFolder() {
        Path path;
        boolean creado = false;
        path = Paths.get(getDirTrabajo(), "");
        if (System.getProperty("os.name").toUpperCase().contains("XP")) {
            path = Paths.get(getUserHome(), "");
        }
        if (!Files.exists(path, LinkOption.NOFOLLOW_LINKS)) {
            try {
                Files.createDirectory(path);
                path = path.toAbsolutePath();
                System.out.println("\n" + path + " directorio creado.");
                return true;
            } catch (NoSuchFileException e) {
                creado = false;
                System.err.println("\nDirectory creation failed:\n" + e);
            } catch (FileAlreadyExistsException e) {
                creado = false;
                System.err.println("\nDirectory creation failed:\n" + e);
            } catch (IOException e) {
                creado = false;
                System.err.println("\nDirectory creation failed:\n" + e);
            }
        }
        return creado;
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {

    }

}
