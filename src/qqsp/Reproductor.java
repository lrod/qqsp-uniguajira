/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package qqsp;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javazoom.jl.decoder.JavaLayerException;
import javazoom.jl.player.Player;

/**
 *
 * @author ubuntu
 */
public class Reproductor {

    private Player currentPlayer;
    private int estado;
    private Thread hilo;
    int ct = 0;

    public Reproductor() {

    }

    public void playSound(final String sound, boolean stop) {
        if (stop && estado == 1) {
            System.out.println(hilo.getName());
            hilo.stop();
            estado = 0;
        }
        hilo = new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    InputStream stream = getClass().getResourceAsStream(sound);
                    Player player = new Player(stream);
                    estado = 1;
                    player.play();
                } catch (JavaLayerException ex) {
                    Logger.getLogger(Reproductor.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }, "hilo" + ct++);
        hilo.start();

    }

}
