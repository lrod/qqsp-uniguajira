/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package qqsp;

import java.awt.BorderLayout;
import java.awt.Image;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

/**
 *
 * @author ubuntu
 */
class PanelInstruccion extends JPanel {

    private final Aplication app;
    private final Image imagen;
    
    
    public PanelInstruccion(Aplication app, Image imagen) {
        this.app = app;
        this.imagen = imagen;
        setLayout(new BorderLayout());
        initComponents();
    }
    
    private void initComponents() {
        JLabel lb = new JLabel();
        lb.setHorizontalAlignment(SwingConstants.CENTER);
        lb.setHorizontalTextPosition(SwingConstants.CENTER);
        lb.setText("<html><h1><font size=+3>INSTRUCCIONES</font></h1>"
                + "<p><font size=+1 color=white>Instrucciones del juego<font></p></html>");
        add(lb, BorderLayout.CENTER);
        
        JButton btVolver = new JButton("Volver");
        btVolver.addActionListener(app);
        btVolver.setActionCommand(Aplication.AC_MOSTRAR_PRESENTACION);
        add(btVolver, BorderLayout.SOUTH);
    }
    
}
