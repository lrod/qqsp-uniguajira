/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package qqsp;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.DefaultButtonModel;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.SwingConstants;

/**
 *
 * @author ubuntu
 */
public class BotonRespuesta extends JButton {

    public static final Color COLOR_BG_DF = new Color(255, 165, 92);

    public BotonRespuesta() {
        this(null, null);
    }

    public BotonRespuesta(Icon icon) {
        this(null, icon);
    }

    public BotonRespuesta(String text) {
        this(text, null);
    }

    public BotonRespuesta(Action a) {
        this();
        setAction(a);
    }

    public BotonRespuesta(String text, Icon icon) {
        setModel(new DefaultButtonModel());
        init(text, icon);
        if (icon == null) {
            return;
        }
        setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1));
        setBackground(COLOR_BG_DF);
        setContentAreaFilled(false);
        setFocusPainted(false);
//        setVerticalAlignment(SwingConstants.CENTER);
//        setAlignmentY(Component.TOP_ALIGNMENT);
//        setVerticalTextPosition(SwingConstants.BOTTOM);
//        setHorizontalTextPosition(SwingConstants.CENTER);
//        setVerticalTextPosition(SwingConstants.TOP);
        setHorizontalTextPosition(SwingConstants.CENTER);
        initShape();
        makeListeners();
    }
    protected Shape shape, base;

    private void makeListeners() {
        addMouseListener(new MouseAdapter() {

            @Override
            public void mouseEntered(MouseEvent e) {
                super.mouseEntered(e); //To change body of generated methods, choose Tools | Templates.
                setForeground(COLOR_BG_DF.darker());
                repaint();
                System.out.println("entered");
            }

            @Override
            public void mouseExited(MouseEvent e) {
                super.mouseExited(e); //To change body of generated methods, choose Tools | Templates.
                setForeground(COLOR_BG_DF);
                repaint();
                System.out.println("exited");
            }
        });
    }

    protected void initShape() {
        if (!getBounds().equals(base)) {
            Dimension s = getPreferredSize();
            Rectangle b = getBounds();
            int[] px = {0, (int) (b.width * .2), (int) (b.width * .8), b.width-1, (int) (b.width * .8), (int) (b.width * .2)};
            int[] py = {(int) (b.height * .5), 0, 0, (int) (b.height * .5), b.height-1, b.height-1};
            shape = new Polygon(px, py, px.length);
        }
    }

    @Override
    public Dimension getPreferredSize() {

        Icon icon = getIcon();
        Insets i = getInsets();
        int iw = (icon != null) ? Math.max(icon.getIconWidth(), icon.getIconHeight()) : 100;
        return new Dimension(iw + i.right + i.left, iw + i.top + i.bottom);
    }

    @Override
    protected void paintBorder(Graphics g) {
        initShape();
        Graphics2D g2 = (Graphics2D) g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);

        g2.setColor(getBackground());
        g2.setStroke(new BasicStroke(1.0f));

        g2.draw(shape);
//        g2.drawString(getText(), 10, 20);
//        g2.setColor(Color.orange);
//        g2.draw(shape);
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_OFF);

    }

    @Override
    public boolean contains(int x, int y) {
        initShape();
        return shape.contains(x, y);
        //or return super.contains(x, y) && ((image.getRGB(x, y) >> 24) & 0xff) > 0;
    }
}
