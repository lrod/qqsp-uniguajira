/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package qqsp;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.prefs.Preferences;

/**
 *
 * @author ballestax
 */
public class Configuration {

    private Preferences horPrefs;
    private Properties configuration;
    public static final String ARCHIVO_RECIENTE = "arc_reciente";
    private String path;
    private String NAME = "configuracion.ini";
    public static final String DATABASE_DRIVER = "db.driver";
    public static final String DATABASE_PREFIJO = "db.prefijo";
    public static final String DATABASE_URL = "db.url";
    public static final String DATABASE_USER = "db.user";
    public static final String DATABASE_PASSWORD = "db.pass";

    private Configuration() {
        try {
            horPrefs = Preferences.userRoot().node(Aplication.PREFERENCES);
            configuration = new Properties();
        } catch (Exception e) {
            // thrown when running unsigned JAR
            horPrefs = null;
        }
    }

    public static Configuration getInstancia() {
        return Configuration.ConfigurationInstance.INSTANCIA;
    }

    private void loadDefault() {
        setProperty(DATABASE_DRIVER, "org.sqlite.JDBC");
        setProperty(DATABASE_PREFIJO, "jdbc:sqlite");
        setProperty(DATABASE_URL, "dbelect");
        save();
    }

    private static class ConfigurationInstance {

        private static final Configuration INSTANCIA = new Configuration();
    }

    public String loadPreferences(String llave, String valorDefecto) {
        return horPrefs.get(llave, valorDefecto);
    }

    public void savePreferences(String llave, String valor) {
        if (llave != null && valor != null) {
            horPrefs.put(llave, valor);
        }
    }

    public void cleanPreferences() {
        try {
            horPrefs.clear();
            horPrefs.flush();
        } catch (Exception e) {
            System.err.println("(Exc)limpiarPreferencias(): " + e);
        }
    }

    public void savePreferences() {
        try {
            horPrefs.flush();
        } catch (Exception e) {
            System.err.println("(Exc)savePreferencias(): " + e);
        }
    }

    public void load() {
        try {
            File arc = new File(path + File.separator + NAME);
            FileInputStream fis = new FileInputStream(arc);
            configuration.load(fis);
            System.out.println("Cargando la configuracion..." + arc.getAbsolutePath());
        } catch (IOException e) {
            System.err.println("No se encuentra el archivo de configuracion.");
            loadDefault();
        }
    }

    public synchronized void save() {
        System.out.println("Guardando la configuracion...");
        try {
            File arc = new File(path + File.separator + NAME);
            FileOutputStream fos = new FileOutputStream(arc);
            configuration.store(fos, "Configuration Formularios");
            System.out.println("Guardando la configuracion..." + arc.getAbsolutePath());
        } catch (IOException e) {
            System.err.println("Error al save la configuracion: " + e.getMessage());
        }
    }

    public String getProperty(String property) {
        return configuration.getProperty(property);
    }

    public String getProperty(String property, String propDefault) {
        return configuration.getProperty(property, propDefault);
    }

    public int getProperty(String property, int propDefault) {
        int valor = propDefault;
        try {
            valor = Integer.parseInt(configuration.getProperty(property, "" + propDefault));
        } catch (Exception e) {
        }
        return valor;
    }

    public void setProperty(String property, String valor) {
        configuration.setProperty(property, valor);
    }

    public Set<Map.Entry<Object, Object>> getConfiguration() {
        return configuration.entrySet();
    }

    public void setPath(String path) {
        this.path = path;
    }
}
