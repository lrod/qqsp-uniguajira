/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package qqsp;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JPanel;

/**
 *
 * @author ubuntu
 */
public class PnPresentacion extends JPanel implements ActionListener {

    private Image image;
    private JButton btInstruccion;
    private JButton btJugar;
    private static final String AC_NUEVO_JUEGO = "ac_nuevo_juego";
    private static final String AC_INSTRUCCION_JUEGO = "ac_instruccion_juego";
    private Aplication app;

    public PnPresentacion(Aplication app, Image image) {
        this.app = app;
        this.image = image;
        setLayout(new GridBagLayout());
        initComponents();
    }

    private void initComponents() {
        btJugar = new JButton("<html><font size=+3 color=white>JUGAR</font></html>");
        btJugar.setBackground(new Color(204, 41, 0));
        btJugar.addActionListener(this);
        btJugar.addActionListener(app);
        btJugar.setFocusPainted(false);
        btJugar.setActionCommand(AC_NUEVO_JUEGO);
        btInstruccion = new JButton("<html><font size=+3 color=white>INSTRUCCIONES</font></html>");
        btInstruccion.addActionListener(this);
        btInstruccion.addActionListener(app);
        btInstruccion.setBackground(new Color(204, 41, 0));
        btInstruccion.setFocusPainted(false);
        btInstruccion.setActionCommand(AC_INSTRUCCION_JUEGO);
        add(btJugar, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(5, 5, 5, 5), 100, 30));
//        add(Box.createHorizontalStrut(200), new GridBagConstraints(5, 5, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(5, 5, 5, 5), 100, 30));
        add(btInstruccion, new GridBagConstraints(1, 8, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(25, 5, 5, 5), 100, 30));
//        add(Box.createHorizontalStrut(200), new GridBagConstraints(5, 7, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(5, 5, 5, 5), 100, 30));
        System.err.println(getComponentCount());

    }

    private void acomodar() {
        Rectangle bd = getBounds();
        int x = bd.width - 400;
        int y = (int) (bd.height - bd.height * .6);
        System.err.println(x + "-" + y);
//        btJugar.setLocation(x, y);
//        btInstruccion.setLocation(x, y + 130);
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponents(g);
        Rectangle bd = getBounds();
        g.drawImage(image, 0, 0, bd.width, bd.height, this);
//        acomodar();
    }

//    @Override
//    public void paint(Graphics g) {
//        super.paint(g); //To change body of generated methods, choose Tools | Templates.
//       
//    }
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals(AC_NUEVO_JUEGO)) {

        } else if (e.getActionCommand().equals(AC_INSTRUCCION_JUEGO)) {

        }
    }
}
