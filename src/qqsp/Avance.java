/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package qqsp;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.font.FontRenderContext;
import java.awt.font.LineMetrics;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import javax.swing.JLabel;

/**
 *
 * @author ubuntu
 */
public class Avance extends JLabel {

    private int nPreg;
    private int sel;
    private ArrayList lista;

    public Avance(ArrayList lista) {
        this.lista = lista;
        nPreg = lista.size();
        sel = nPreg - 1;
    }

    public void avanzar() {
        sel--;
        repaint();
    }

    @Override
    public void setVisible(boolean aFlag) {
        super.setVisible(aFlag); //To change body of generated methods, choose Tools | Templates.
        repaint();

    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        Rectangle bnd = getBounds();
        int h = (int) (bnd.height * 0.8);
        int hc = h / nPreg;
        int py = (bnd.height / 2) - ((h + (5 * (nPreg - 1))) / 2);
        int bw = (int) (bnd.width * .8);
        int px = (bnd.width / 2) - (bw / 2);
        Graphics2D g2 = (Graphics2D) g;
        g2.setColor(Color.orange);
        g2.setStroke(new BasicStroke(2.0f));
        Font fnt = new Font("Arial", 1, 25);
        for (int i = 0; i < nPreg; i++) {
            drawRect(g2, px, py + (hc * i) + (5 * (i + 1)), bw, hc, (nPreg - i) + ". " + lista.get(nPreg - i - 1), fnt,
                    i == sel ? true : false, i < sel);
        }
        g2.setColor(Color.red);

//        g2.drawOval(0, 0, 200, 200);
//        g2.translate(100, 100);
//        g2.setColor(Color.white);
//        Point pnt = getCoordenadas();
//        g2.drawLine(0, 0, pnt.x, pnt.y);
    }

    public void drawRect(Graphics2D g, int x, int y, int w, int h, String lb, Font f, boolean selected, boolean skip) {
        g.setColor(selected ? Color.blue : Color.gray);
        g.fillRect(x, y, w, h);
        g.setColor( Color.orange);
        g.drawRect(x, y, w, h);
        g.setColor(skip ? Color.white : Color.orange);
        Font fnt = f;
        float tf = f.getSize();
        FontRenderContext frc = g.getFontRenderContext();
        LineMetrics lineMetrics = f.getLineMetrics(lb, frc);
        Rectangle2D stringBnd = f.getStringBounds(lb, frc);
        boolean band = true;
        while (band) {
            g.setFont(fnt);
            stringBnd = fnt.getStringBounds(lb, frc);
            if (stringBnd.getWidth() > w || stringBnd.getHeight() > h) {
                fnt = fnt.deriveFont(tf--);
                band = true;
            } else {
                band = false;
            }
        }
        int px = x + (int) ((w / 2) - (stringBnd.getWidth() / 2));
        int py = y + (int) (stringBnd.getHeight() + lineMetrics.getLeading());
        g.drawString(lb, px, py);
    }

}
