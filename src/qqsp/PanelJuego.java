/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package qqsp;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

/**
 *
 * @author ubuntu
 */
public class PanelJuego extends JPanel {

    private final Aplication app;
    private final ControlJuego ctrl;
    private PanelPregunta panelPregunta;
    private Avance avance;

    public PanelJuego(Aplication app, ControlJuego ctrl) {
        this.app = app;
        this.ctrl = ctrl;
//        setLayout(new GridBagLayout());
        setLayout(new BorderLayout());
        initComponents();
    }

    public PanelJuego(Aplication app) {
        this.app = app;
        this.ctrl = null;
        setLayout(new BorderLayout());
        initComponents();
    }

    private void initComponents() {
        Pregunta preg = new Pregunta("Cuantos dias tiene un año bisiesto", new String[]{"365", "364", "366", "370"}, 2);
        panelPregunta = new PanelPregunta();
//        panelPregunta.hacerPregunta(preg);
        JPanel pn1 = new JPanel(new BorderLayout());
        pn1.setBackground(Color.red);
        JPanel pn2 = new JPanel(new BorderLayout());
        pn2.setBackground(Color.blue);

        avance = new Avance(new ArrayList());
        avance.setPreferredSize(new Dimension(250, 600));

        JButton btRetirar = new JButton("Retirarse");
        btRetirar.setBorder(new EmptyBorder(20, 10, 20, 10));
        JLabel labelN = new JLabel();
//        labelN.setBorder(new EmptyBorder(200, 10, 200, 10));
        labelN.setOpaque(true);
        labelN.setBackground(Color.green);

        pn1.add(panelPregunta, BorderLayout.SOUTH);
        pn1.add(labelN, BorderLayout.NORTH);
        pn2.add(avance, BorderLayout.CENTER);
        pn2.add(btRetirar, BorderLayout.SOUTH);
        add(pn1, BorderLayout.CENTER);
        add(pn2, BorderLayout.EAST);
    }

    public final void siguientePregunta() {
        Pregunta pregunta = app.getControl().getPregunta();
        panelPregunta.hacerPregunta(pregunta);
    }

}
